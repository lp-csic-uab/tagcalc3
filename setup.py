# exWx/setup.py
#
## python setup.py py2exe 
#
from distutils.core import setup
#noinspection PyUnresolvedReferences
import py2exe
#
####################################
PyAppName = "tcalc_gui.pyw"
AppName = "TagCalc"
AppVers = "1.7"
Icon = 'images//tcalc48.ico'
####################################
#
setup(
    windows = [ 
              {'script': PyAppName,
              'icon_resources':[(0, Icon)],
               'dest_base' : "%s %s" %(AppName, AppVers),    
               'version' : AppVers,
               'company_name' : "JoaquinAbian",
               'copyright' : "No Copyrights",
               'name' : AppName 
              }
              ],
            
    options = {
              'py2exe': {
                        #'packages' :    [],
                        #'includes':     [],
                        'excludes':     ['matplotlib', 'email', 'testing', 'fft',
                                         'Tkconstants','Tkinter', 'tcl'
                                        ],
                        'ignores':       ['wxmsw26uh_vc.dll'],
                        'dll_excludes': ['libgdk_pixbuf-2.0-0.dll',
                                         'libgdk-win32-2.0-0.dll',
                                         'libgobject-2.0-0.dll'
                                        ],
                        'compressed': 1,
                        'optimize':2,
                        'bundle_files': 1
                        }
              },
    zipfile = None,
    data_files = [("", ["INSTALL.txt",   #Installer last dialog
                        "README.txt",    #Aplication README and help
                        "LICENCE.TXT",   #Licence File (GPL v3)
                        "msvcp90.dll",
                        "C:\\Python26/Lib/site-packages/wx-2.8-msw-unicode/wx/gdiplus.dll" #,
                        #"C:\\Python26/lib/site-packages/numpy/core\umath.pyd"
                        ]
                  )
                 ]
    )
