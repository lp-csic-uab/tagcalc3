#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
#
"""
calc_class_grid
"""
#
import wx
import wx.grid as Grid
#
#
class MegaGrid(Grid.Grid):
    """"""
    #noinspection PyArgumentList
    def __init__(self, *args, **kwargs):
        Grid.Grid.__init__(self, *args, **kwargs)
        self.CreateGrid(6, 3)
        self.col_names = ["Sequence", "Error", "Prob"]
        self.SetRowLabelSize(70)
        self.SetColSize(1, 65)
        self.SetColSize(2, 65)
        self.data = [(row, ('', '', '')) for row in range(6)]
                  
        self._rows = self.GetNumberRows()
        self._cols = self.GetNumberCols()
       
        self.Bind(Grid.EVT_GRID_LABEL_RIGHT_CLICK, self.OnLabelRightClick)
        
        self.refresh()
    #
    def OnLabelRightClick(self, evt):
        """"""
        row, col = evt.GetRow(), evt.GetCol()
        if row == -1 and col == -1:
            self.savePopup()
            return
        if row == -1: self.colPopup(col)
        if col == -1: self.rowPopup(row)
    #
    def savePopup(self):
        """"""
        print('in save')
        saveID = wx.NewId()
        menu = wx.Menu()
        #noinspection PyArgumentList
        menu.Append(saveID, "Save")
        self.Bind(wx.EVT_MENU, self.on_save, id=saveID)
        self.PopupMenu(menu)
        menu.Destroy()

    #noinspection PyUnusedLocal
    def on_save(self, evt):
        """"""
        dlg = wx.FileDialog(self, "Select File", defaultDir = "C:/",
                    wildcard = "Excel (tab separated)(.xls)|*.xls|All(*.*)|*.*",
                    style=wx.SAVE|wx.OVERWRITE_PROMPT)
        #
        if dlg.ShowModal() == wx.ID_OK:
            target = dlg.GetPath()
            cols = self.GetNumberCols()
            rows = self.GetNumberRows()
            save_data = []
            for row in range(rows):
                #noinspection PyArgumentList
                row_list = [self.GetCellValue(row, column)
                                                for column in range(cols)]
                line = '\t'.join(row_list)
                save_data.append(line)
            txt = '\n'.join(save_data)
            #
            open(target,'w').write(txt)
            
    def rowPopup(self, row):
        """display a popup menu when a row label is right clicked"""
        deleteID = wx.NewId()

        if not self.GetSelectedRows():
            self.SelectRow(row)

        menu = wx.Menu()
        #noinspection PyArgumentList
        menu.Append(deleteID, "Delete Row(s)")

        def delete(event, self=self):
            rows = self.GetSelectedRows()
            rows.reverse()
            for item in rows:
                self.DeleteRows(item)

        self.Bind(wx.EVT_MENU, delete, id=deleteID)
        self.PopupMenu(menu)
        menu.Destroy()
        return

    #noinspection PyArgumentList
    def colPopup(self, col):
        """Display a popup menu when a column label is right clicked

        """
        menu = wx.Menu()
        sortIDd = wx.NewId()
        sortIDr = wx.NewId()
        
        self.SelectCol(col)
        cols = self.GetSelectedCols()
        self.Refresh()
        menu.Append(sortIDd, "Sort Column - -> +")
        menu.Append(sortIDr, "Sort Column + -> -")
         
        def sortd(event, self=self, col=col):
            self.SortColumn(col, False)
        
        def sortr(event, self=self, col=col):
            self.SortColumn(col, True)
   
        if len(cols) == 1:
            self.Bind(wx.EVT_MENU, sortd, id=sortIDd)
            self.Bind(wx.EVT_MENU, sortr, id=sortIDr)
        
        self.PopupMenu(menu)
        menu.Destroy()
        return
  
    def SortColumn(self, col, high2low=False):
        """Sort the data based on the column indexed by col
        """
        #noinspection PySimplifyBooleanCheck
        if col == 0: return
        _data = []
        #
        for row in self.data:
            row_name, entry = row
            _data.append((float(entry[col]), row))

        _data.sort(reverse=high2low)
        self.data = [row for sort_value, row in _data]
                
        self.refresh()

    #noinspection PyArgumentList
    def refresh(self):
        #self.SetCellValue(-1,-1,"[save]")
        for i in range(len(self.col_names)):
            self.SetColLabelValue(i, self.col_names[i])
            
        for index, row in enumerate(self.data):
            self.SetRowLabelValue(index, str(row[0]))
            for i in range(len(self.data[0][1])):
                self.SetCellValue(index, i, str(self.data[index][1][i]))
#
#
#
if __name__ == "__main__":
    
    class MyGridFrame(wx.Frame):
        def __init__(self, *args, **kwds):
            wx.Frame.__init__(self, *args, **kwds)
            self.grid = MegaGrid(self) 
            self.SetSize((340, 170))
            self.SetTitle(" Mi Grid")
    #
    #
    class MyGrid(wx.App):
        def OnInit(self):
            wx.InitAllImageHandlers()
            frame = MyGridFrame(None, -1, "")
            self.SetTopWindow(frame)
            frame.Show()
            return 1
    #
    #
    grid = MyGrid(0)
    grid.MainLoop()

