#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
#
"""
tcalc_gui (tagcalc_17)
15 agosto 2010
"""
#
import wx
from tcalc_algoritmo import get_tags
from tcalc_class_grid import MegaGrid
from tcalc_images import ICON
from commons.mcalc import MiniCalc
from commons.iconic import Iconic
#
FRAME_TITLE = "  TAG CALCULATOR 1.7"
#
dict_precision = dict(very_high = 0.05, high = 0.1,
                      mean = 0.5, low = 1, very_low=2.5)
#
#
#noinspection PyUnusedLocal
class CalcFrame(wx.Frame):
    """"""
    def __init__(self, *args, **kwds):
        wx.Frame.__init__(self, *args, **kwds)
        self.calc = MiniCalc(self)
        self.Bind(wx.EVT_BUTTON, self.on_bt_calc, self.calc.bt_calc)
    #
    def on_bt_calc(self, evt):
        """calculates peptide mass."""
        # works with residues not with complete mass
        self.calc.water = False     
        self.calc.calc()
#
#
#noinspection PyUnusedLocal
class MyCalcFrame(wx.Frame, Iconic):
    """"""
    def __init__(self, *args, **kwds):
        """"""
        kwds["style"] = wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        Iconic.__init__(self, icon=ICON)
        #
        self.calc_number = 0
        self.mass = 0
        self.avg = 'mono'
        #
        size = (45, 20)
        size1 = (55, 20)
       
        self.wind = MegaGrid(self, -1, size=(1, 1))
        self.lb_ion_1 = wx.StaticText(self, -1, "ION_1", size=size, style=wx.ALIGN_CENTRE)
        self.lb_ion_2 = wx.StaticText(self, -1, "ION_2", size=size, style=wx.ALIGN_CENTRE)
        self.bt_search = wx.Button(self, -1, "SEARCH", size=size)
        self.bt_prcsn = wx.StaticText(self, -1, "Precision ", size=size, style=wx.ALIGN_RIGHT)
        choices = list(dict_precision.keys())
        self.cbx_prcsn = wx.ComboBox(self, -1, "low", choices=choices,
                                     size=size, style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.CB_SORT)
        self.tc_ion_1 = wx.TextCtrl(self, -1, "", size=size)
        self.tc_ion_2 = wx.TextCtrl(self, -1, "", size=size)
        self.tc_val = wx.TextCtrl(self, -1, "", size=size, style=wx.TE_READONLY)
        self.lb_mx = wx.StaticText(self, -1, "Max AA ", size=size1, style=wx.ALIGN_RIGHT)
        self.cbx_aa = wx.ComboBox(self, -1, "3",
                                   choices=["1", "2", "3", "4", "5", "6", "7"],
                                   size=size1, style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.CB_SORT)
        self.bt_iso = wx.Button(self, -1, "iso", size=(20, 21))
        self.bt_cal = wx.Button(self, -1, "cal", size=(20, 21))
        
        self.__set_properties()
        self.__do_layout()

        self.Bind(wx.EVT_BUTTON, self.on_search, self.bt_search)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_search, self.tc_ion_1)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_search, self.tc_ion_2)
        self.Bind(wx.EVT_BUTTON, self.on_avg, self.bt_iso)
        self.Bind(wx.EVT_BUTTON, self.on_calc, self.bt_cal)
    #
    def __set_properties(self):
        """"""
        self.SetTitle(FRAME_TITLE)
        self.SetPosition((50, 50))
        #
        self.SetSize((320, 230))
        self.SetMaxSize((320, 750))
        self.SetMinSize((180, 230))
        #
        self.bt_prcsn.SetMinSize((60, 15))
        self.bt_prcsn.SetToolTip("'precision'")
        self.lb_mx.SetMinSize((60, 15))
        self.lb_mx.SetToolTip("'number of amino acids'")
        
        self.SetBackgroundColour(wx.Colour(255, 185, 5))
        self.lb_ion_1.SetBackgroundColour(wx.Colour(10, 255, 5))
        self.lb_ion_2.SetBackgroundColour(wx.Colour(10, 255, 5))
        self.bt_search.SetBackgroundColour(wx.Colour(255, 0, 0))
        self.tc_ion_1.SetBackgroundColour(wx.Colour(255, 155, 220))
        self.tc_ion_2.SetBackgroundColour(wx.Colour(255, 155, 220))
        self.cbx_prcsn.SetBackgroundColour(wx.Colour(255, 155, 220))
        self.cbx_aa.SetBackgroundColour(wx.Colour(255, 155, 220))
        self.tc_val.SetBackgroundColour(wx.Colour(255, 255, 100))
    #
    #noinspection PyArgumentList
    def __do_layout(self):
        # begin wxGlade: MyFrame.__do_layout
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        grid_sizer = wx.FlexGridSizer(2, 7, 1, 1)
        sizer_1.Add(self.wind, 1, wx.ALL|wx.EXPAND, 5)
        grid_sizer.Add(self.lb_ion_1, 0, wx.ALL|wx.EXPAND|
                       wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL, 2)
        grid_sizer.Add(self.lb_ion_2, 0, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_VERTICAL, 2)
        grid_sizer.Add((5, 5))
        grid_sizer.Add(self.bt_search, 0, wx.ALL|wx.EXPAND, 2)
        grid_sizer.Add(self.bt_prcsn, 0, wx.ALL|wx.EXPAND, 5)
        grid_sizer.Add(self.cbx_prcsn, 0, wx.ALL|wx.EXPAND, 2)
        grid_sizer.Add(self.bt_iso, 0, wx.ALL, 2)
        grid_sizer.Add(self.tc_ion_1, 0, wx.LEFT|wx.BOTTOM|wx.EXPAND, 3)
        grid_sizer.Add(self.tc_ion_2, 0, wx.BOTTOM|wx.EXPAND, 3)
        grid_sizer.Add((5, 5))
        grid_sizer.Add(self.tc_val, 0, wx.BOTTOM|wx.EXPAND, 3)
        grid_sizer.Add(self.lb_mx, 0, wx.ALL|wx.EXPAND, 5)
        grid_sizer.Add(self.cbx_aa, 0, wx.ALL|wx.EXPAND, 2)
        grid_sizer.Add(self.bt_cal, 0, wx.ALL, 2)
        
        sizer_1.Add(grid_sizer, 0, wx.EXPAND, 0)
        self.SetSizer(sizer_1)
        self.Layout()
    #
    def set_mass(self):
        """"""
        val_a = self.string_to_float(self.tc_ion_1.GetValue())
        val_b = self.string_to_float(self.tc_ion_2.GetValue())
        #
        self.mass = abs(val_b - val_a)
        self.tc_val.Clear()
        self.tc_val.WriteText(str(self.mass))
    #
    def string_to_float(self, string):
        """converts a string into a float"""
        try:
            return float(string)
        except ValueError:
            return 0
    #
    def on_search(self, event): # wxGlade: MyFrame.<event_handler>
        """"""
        self.set_mass()
        matches = []
        #
        max_aas = int(self.cbx_aa.GetValue())
        precision = self.cbx_prcsn.GetValue()
        max_error = dict_precision[precision]
        #
        if self.mass > (186.3*max_aas):
            self.adjust_rows(0)
            return
        #
        matches = get_tags(self.mass, max_aas, max_error, mode=self.avg)
        #
        num_data = len(matches)
        self.adjust_rows(num_data)
        #
        for i in range(num_data):
            for j in range(len(matches[0])):
                #noinspection PyArgumentList
                self.wind.SetCellValue(i, j, matches[i][j])
        #
        self.wind.data = []
        for i in range(len(matches)):
            self.wind.data.append((i+1, matches[i]))
        #print self.wind.data
        self.wind.refresh()
    #
    def adjust_rows(self, number):
        """"""
        actual = self.wind.GetNumberRows()
        if actual > 1:
            #noinspection PyArgumentList
            self.wind.DeleteRows(1, actual)
        #    
        if number > 0:
            self.wind.AppendRows(number-1)
        else:
            self.wind.data = [(1, ('-', '-', '-'))]
            self.wind.refresh()       
    #
    def on_avg(self, event): # wxGlade: MyFrame.<event_handler>
        """Changes mode from 'mono' to 'average' and vice versa"""
        if self.avg == 'mono':
            self.bt_iso.SetBackgroundColour(wx.Colour(255, 0, 0))
            self.bt_iso.SetLabel('avg')
            self.avg = 'avg'
        else:
            self.bt_iso.SetBackgroundColour(wx.Colour(200, 200, 200))
            self.bt_iso.SetLabel('iso')
            self.avg = 'mono'
    #
    def on_calc(self, event):
        """clones calculator"""
        self.calc_number += 1
        title = " Calculator %i" % self.calc_number
        style = wx.CAPTION|wx.SYSTEM_MENU|wx.CLOSE_BOX|wx.FRAME_TOOL_WINDOW
        calculator = CalcFrame(None, -1, title, size=(278, 100), style=style)
        xpos, ypos = self.GetPosition()
        xsize, ysize = self.GetSize()
        win_size = wx.GetDisplaySize()
        offset = 10 * (self.calc_number-1)
        #
        if (xpos+xsize+270) > win_size.GetWidth():        #off by the right
            if ypos+ysize+100 > win_size.GetHeight():     #off by the bottom
                new_pos = (xpos-270-offset, ypos+ysize-100-offset)  #go left
            else:
                new_pos = (xpos+xsize-270+offset, ypos+ysize+offset)  #go down
        else:
            new_pos = (xpos+xsize+offset, ypos+ysize-100+offset)      #go right
        
        calculator.SetPosition(new_pos)
        
        calculator.Show()
#
#
class MyCalcApp(wx.App):
    def OnInit(self):
        wx.InitAllImageHandlers()
        frame = MyCalcFrame(None, -1, "")
        self.SetTopWindow(frame)
        frame.Show()
        return 1


if __name__ == "__main__":
    my_calculator = MyCalcApp(0)
    my_calculator.MainLoop()
