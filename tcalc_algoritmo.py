#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
#
"""
calc_algorithm (calc_16)
15 agosto 2010
"""
#
#
from commons.mass import prob_aa, get_mass
#            
#list of amino acids sorted by increasing mass (important)
aa_list = ['G', 'A', 'S', 'P', 'V', 'T', 'C', 'J', 'N', 'D', 'Q', 'K',
           'E', 'M', 'H', 'm', 'F', 'R', 'c', 'Y', 's', 't', 'W', 'y']
#
#
def get_tags(search_mass, max_aas, err, mode='mono'):
    """Determines sequences fitting a given mass."""
    seeds = aa_list[:]
    matches = []
    for i in range(max_aas-1):
        seeds, matches = grow_seed(seeds, matches, search_mass, err, mode)
    return matches
#
#
def grow_seed(seeds, matches, search_mass, err, mode='mono'):
    """Makes the seeds grow with new amino acids.
    
    returns:
        new_seeds = list of sequences with mass below <search_mass>
        matches = list of sequences reaching <search_mass> under a given error range.

    """
    new_seeds = []
    limit = float(search_mass) + err
    #print 'limit', limit
    #print seed
    for seed in seeds:
        #list index of the last aa added to the seed
        index = aa_list.index(seed[-1])
        for aa in aa_list[index:]:
            new_item = seed + aa
            mass = get_mass(new_item, mode=mode, water=False)
            #if mass is higher than the limit, stop
            if mass > limit:
                break              
            diff = search_mass - mass
            abs_diff = abs(diff)
            if abs_diff < err:
                # set the precision limit, more is not necessary
                if abs_diff < 0.001:
                    abs_diff = 0.001
                probability = sequence_probability(new_item)/(abs_diff**0.5)
                matches.append((new_item, '%+.3f' % diff,'%.2f' % probability))
            if abs_diff > 56:
                new_seeds.append(new_item)
                
    return new_seeds, matches
#
#
def sequence_probability(sequence):
    """Calculates occurrence probability for an amino acid composition
    """
    length = len(sequence)
    prob_final = 1.0
    for atom in sequence:
        prob_final *= prob_aa[atom]
    return prob_final**(1./length)


if __name__ == '__main__':
    
    from time import time
    mass = 663.5
    AA = 7
    error = 0.1
    peptide = 'GASPVTCXLINDQKEMHmFRcYstWy'
    #
    time0 = time()
    #
    final_seed = get_tags(mass, AA, error)
    #
    time1 = time()
    #
    for idx, tag in enumerate(final_seed):
        print(idx, str(tag))
    print('search time = %f)' % (time1-time0))
    print('peptide mass = %f' % get_mass(peptide, water=False)) #3387.306985


#    C:\Python26\python.exe C:/Python26/programas/_tagcalc/tcalc_algoritmo.py
#    0 ('JKKFF', '+0.089', '0.17')
#    1 ('GPPKKR', '+0.082', '0.19')
#    2 ('AAKKKH', '+0.082', '0.19')
#    3 ('APJJJR', '+0.057', '0.29')
#    4 ('ATJJKH', '+0.093', '0.19')
#    5 ('SPPJJR', '+0.093', '0.20')
#    6 ('SVVJKH', '+0.093', '0.18')
#    7 ('PPVTJR', '+0.093', '0.19')
#    8 ('PPVNKK', '+0.093', '0.17')
#    9 ('PVVVJR', '+0.057', '0.27')
#    10 ('PVJJJQ', '+0.068', '0.24')
#    11 ('PVJJJK', '+0.032', '0.37')
#    12 ('PJJJJN', '+0.068', '0.25')
#    13 ('VVVTKH', '+0.093', '0.17')
#    14 ('GGPPVKK', '+0.093', '0.19')
#    15 ('GGPJJJJ', '+0.068', '0.27')
#    16 ('GAPVJJJ', '+0.068', '0.27')
#    17 ('GPVVVVJ', '+0.068', '0.25')
#    18 ('AAAPPKK', '+0.093', '0.21')
#    19 ('AAPVVJJ', '+0.068', '0.28')
#    20 ('APVVVVV', '+0.068', '0.25')
#    search time = 0.557000)
#    peptide mass = 3387.306985
#
#    Process finished with exit code 0
