
---

**WARNING!**: This is the *Old* source-code repository for TagCalc3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/tagcalc3/) located at https://sourceforge.net/p/lp-csic-uab/tagcalc3/**  

---  
  
  
# TagCalc3 program

Calculates groups of aminoacids that adjust to a mass difference (Python 3 version).


---

**WARNING!**: This is the *Old* source-code repository for TagCalc3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/tagcalc3/) located at https://sourceforge.net/p/lp-csic-uab/tagcalc3/**  

---  
  

